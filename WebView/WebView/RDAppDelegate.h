//
//  RDAppDelegate.h
//  WebView
//
//  Created by Rahul Datta on 7/14/14.
//  Copyright (c) 2014 Rahul Datta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
