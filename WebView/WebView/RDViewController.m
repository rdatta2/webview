//
//  RDViewController.m
//  WebView
//
//  Created by Rahul Datta on 7/14/14.
//  Copyright (c) 2014 Rahul Datta. All rights reserved.
//

#import "RDViewController.h"

@interface RDViewController ()

@end

@implementation RDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	NSString *urlString = @"https://www.google.com";
	NSURL *url = [NSURL URLWithString:urlString];
	NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
	[_viewWeb loadRequest:requestURL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
